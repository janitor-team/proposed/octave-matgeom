## Copyright (C) 2019 David Legland
## All rights reserved.
## 
## Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are met:
## 
##     1 Redistributions of source code must retain the above copyright notice,
##       this list of conditions and the following disclaimer.
##     2 Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in the
##       documentation and/or other materials provided with the distribution.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ''AS IS''
## AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
## ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR
## ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
## DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
## SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
## CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
## OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## The views and conclusions contained in the software and documentation are
## those of the authors and should not be interpreted as representing official
## policies, either expressed or implied, of the copyright holders.

function trans = fitAffineTransform2d(pts1, pts2)
%FITAFFINETRANSFORM2D Fit an affine transform using two point sets.
%   TRANS = fitAffineTransform2d(PTS1, PTS2)
%
%   Example
%   N = 10;
%   pts = rand(N, 2)*10;
%   trans = createRotation(3, 4, pi/4);
%   pts2 = transformPoint(pts, trans);
%   pts3 = pts2 + randn(N, 2)*2;
%   fitted = fitAffineTransform2d(pts, pts2);
%   
%
%   See also
%     transforms2d, transformPoint, transformVector,
%     fitPolynomialTransform2d, registerICP, fitAffineTransform3d
%

% ------
% Author: David Legland
% e-mail: david.legland@inra.fr
% Created: 2009-07-31,    using Matlab 7.7.0.471 (R2008b)
% Copyright 2009 INRA - Cepia Software Platform.


% number of points 
N = size(pts1, 1);
if size(pts2, 1) ~= N
    error('Requires the same number of points for both arrays');
end

% main matrix of the problem
A = [...
    pts1(:,1) pts1(:,2) ones(N,1) zeros(N, 3) ; ...
    zeros(N, 3) pts1(:,1) pts1(:,2) ones(N,1)  ];

% conditions initialisations
B = [pts2(:,1) ; pts2(:,2)];

% compute coefficients using least square
coefs = A\B;

% format to a matrix
trans = [coefs(1:3)' ; coefs(4:6)'; 0 0 1];
