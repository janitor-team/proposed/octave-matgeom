## Copyright (C) 2019 David Legland
## All rights reserved.
## 
## Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are met:
## 
##     1 Redistributions of source code must retain the above copyright notice,
##       this list of conditions and the following disclaimer.
##     2 Redistributions in binary form must reproduce the above copyright
##       notice, this list of conditions and the following disclaimer in the
##       documentation and/or other materials provided with the distribution.
## 
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ''AS IS''
## AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
## ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR
## ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
## DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
## SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
## CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
## OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
## 
## The views and conclusions contained in the software and documentation are
## those of the authors and should not be interpreted as representing official
## policies, either expressed or implied, of the copyright holders.

function varargout = drawEdge3d(varargin)
%DRAWEDGE3D Draw 3D edge in the current axes.
%
%   drawEdge3d(EDGE) draws the edge EDGE on the current axis. 
%   EDGE has the form: [x1 y1 z1 x2 y2 z2]. No clipping is performed.
%   
%   drawEdge3d(AX,...) plots into AX instead of GCA.
%
%   H = drawEdge3d(...) returns a handle H to the line object.
%
%   See also
%   drawLine3d, clipLine3d, drawEdge
%   
% ---------
% author : David Legland 
% INRA - TPV URPOI - BIA IMASTE
% created the 18/02/2005.
%   
%   HISTORY
%   04/01/2007 remove unused variables
%   15/12/2009 "reprecate", and add processing of input arguments

% Parse and check inputs
if numel(varargin{1}) == 1 && ishandle(varargin{1})
    hAx = varargin{1};
    varargin(1) = [];
else
    hAx = gca;
end

% extract edges from input arguments
nCol = size(varargin{1}, 2);
if nCol == 6
    % all parameters in a single array
    edges = varargin{1};
    options = varargin(2:end);
    
elseif nCol == 3
    % parameters are two points, or two arrays of points, of size N*3.
    edges = [varargin{1} varargin{2}];
    options = varargin(3:end);
    
elseif nargin >= 6
    % parameters are 6 parameters of the edge : x1 y1 z1 x2 y2 and z2
    edges = [varargin{1} varargin{2} varargin{3} varargin{4} varargin{5} varargin{6}];
    options = varargin(7:end);
end

% draw edges
h = line(...
    [edges(:, 1) edges(:, 4)]', ...
    [edges(:, 2) edges(:, 5)]', ...
    [edges(:, 3) edges(:, 6)]', 'color', 'b', ...
    'Parent', hAx);
    
% apply optional drawing style
if ~isempty(options)
    set(h, options{:});
end

% return handle to created Edges
if nargout > 0
    varargout = {h};
end
